package com.mr.channel.kbox.channelkboxkaraokebox;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ChannelKboxKaraokeboxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ChannelKboxKaraokeboxApplication.class, args);
	}

}
